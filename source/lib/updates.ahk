updates()
{
Global
Local i
w9x := A_OSType="WIN32_NT" ? FALSE : TRUE
if isBasic := A_AhkVersion < "1.1" ? TRUE : FALSE
	{
	Ptr		:= "Int"
	UPtr		:= "UInt"
	PtrP		:= "IntP"
	UPtrP	:= "UIntP"
	AStr		:= "Str"
	WStr	:= "UInt"
	i := "A_PtrSize"
	if !%i%
	%i% := "4"
	}
PtrSz := A_PtrSize
i := "A_CharSize"
if !%i%
	%i% := A_IsUnicode ? 2 : 1
i := "AW"
if !%i%
	%i% := A_IsUnicode ? "W" : "A"
VarSetCapacity(i, 0)
return TRUE
}

