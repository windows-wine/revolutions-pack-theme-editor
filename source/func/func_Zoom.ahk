; � Drugwash, May 2014
; Inspired by Magnifier from ColorZoomer (2009)
;================================================================
Zoom(hTgt, p="3r", s="200x200")
;================================================================
{
Global Ptr, zoom
Static pt, hwnd
if (pt="")
	VarSetCapacity(pt, 8)		; initialize POINT struct for cursor position
zoom := "1", hid := "1", zf := ""
Loop, Parse, p
	{
	if A_LoopField is number
		zf .= A_LoopField		; zoom factor
	else if A_LoopField=I
		inv=1				; invert
	else if A_LoopField=R
		rnd=1				; round window
	else if A_LoopField=S
		sdw=1				; window shadow
	}
zf := zf ? zf : 3
s := s ? s : "200x200"			; window size (default)
StringSplit s, s, x, %A_Space%%A_Tab%
s2 := s2 ? s2 : s1
t1 := s1/zf, t2 := s2/zf
ControlGetPos, cx, cy, cw, ch,, ahk_id %hTgt%
;hParent := DllCall("GetParent", Ptr, hTgt)
hParent := DllCall("GetAncestor", Ptr, hTgt, "UInt", 2)	; GA_ROOT
;msgbox, zf=%zf% rnd=%rnd% s=%s1%x%s2%`nParent=%hParent% | %hMain%
VarSetCapacity(rect, 16, 0)
DllCall("GetWindowRect", Ptr, hParent, Ptr, &rect)
Loop, 4
	r%A_Index% := NumGet(rect, 4*(A_Index-1), "Int")
ToolTip, %A_Space%, A_ScreenWidth, 0, 20
WinGet, hwnd, ID, ahk_class tooltips_class32
hDCdest := DllCall("GetDC", Ptr, hwnd)
if !sdw
	WinClose, ahk_class SysShadow				; Disable DropShadow
WinMove, ahk_id %hwnd%,, A_ScreenWidth, 0, s1+2, s2+2
hCR := DllCall((rnd ? "CreateEllipticRgn" : "CreateRectRgn"), "Int", 1, "Int", 1, "Int", s1, "Int", s2)
;hCR2 := DllCall("CreateEllipticRgn", "Int", 0, "Int", 0, "Int", s1+2, "Int", s2+2)
;DllCall("CombineRgn", Ptr, hCR, Ptr, hCR, Ptr, hCR2, "UInt", 4)	; RGN_DIFF
DllCall("SetWindowRgn", Ptr, hwnd, Ptr, hCR, "UInt", 1)
;hBr := DllCall("CreateSolidBrush", "UInt", 0xFF0000)
;DllCall("FrameRgn", Ptr, hDCdest, Ptr, hCR, Ptr, hBr, "UInt", 2, "UInt", 2)
;DllCall("DeleteObject", Ptr, hBr)
DllCall("DeleteObject", Ptr, hCR)
;DllCall("DeleteObject", Ptr, hCR2)
ws := DllCall("GetWindowLong", Ptr, hwnd, "Int", -16)	; GWL_STYLE
DllCall("SetWindowLong", Ptr, hwnd, "Int", -16, Ptr, ws|0x800000)	; GWL_STYLE, WS_BORDER
DllCall("SetClassLong"
	, Ptr, hwnd
	, "Int", -12										; GCL_HCURSOR
	, "UInt", DllCall("LoadCursor", "UInt", 0, "UInt", 32515))	; IDC_CROSS
hDC := DllCall("GetDC", Ptr, 0)							; Get screen DC
hDCmem := DllCall("CreateCompatibleDC", Ptr, hDC)			; Create temporary DC
hDCtmp := DllCall("CreateCompatibleDC", Ptr, hDC)			; Create temporary DC
hBM := DllCall("CreateCompatibleBitmap", Ptr, hDC, "Int", t1, "Int", t2)
oBM := DllCall("SelectObject", Ptr, hDCmem, Ptr, hBM)
DllCall("SetStretchBltMode", Ptr, hDCdest, "Int", 3)	; we need precision, not fuzziness so no HALFTONE
hBMtmp := DllCall("CreateCompatibleBitmap", Ptr, hDC, "Int", cw+t1, "Int", ch+t2)
oBMtmp := DllCall("SelectObject", Ptr, hDCtmp, Ptr, hBMtmp)
DllCall("BitBlt"
	, Ptr, hDCtmp
	, "Int", 0
	, "Int", 0
	, "Int", cw+t1
	, "Int", ch+t2
	, Ptr, hDC
	, "Int", r1+cx-(t1/2)
	, "Int", r2+cy-(t2/2)
	, "UInt", 0x00CC0020)
DllCall("ReleaseDC", Ptr, 0, Ptr, hDC)
hRgn := DllCall("CreateRectRgn"
			, "Int", cx-3				; allowing a margin of 3px on each side
			, "Int", cy-3				; to avoid losing zoom window abruptly
			, "Int", cx+cw+3			; when hovering the image
			, "Int", cy+ch+3)
xd := r1-(s1/2)-1, yd := r2-(s2/2)-1, zw := t1*zf, zh := t2*zf	; shortcuts to avoid excessive calculations in loop
Loop
	{
	if !zoom
		break
;	CoordMode, Mouse, Relative
	MouseGetPos, x, y
;	DllCall("GetCursorPos", Ptr, &pt)
;	x := NumGet(pt, 0, "Int"), y := NumGet(pt, 4, "Int")
	; find if Parent window is obscured and contain the loop here
	wnd := DllCall("GetForegroundWindow", Ptr)
	if (wnd != hParent OR !DllCall("IsWindowVisible", Ptr, hTgt))
		continue

	if !DllCall("PtInRegion", Ptr, hRgn, "Int", x, "Int", y)
		{
		if !hid
			{
			WinHide, ahk_id %hwnd%
			hid=1
			}
		continue
		}

	WinMove, ahk_id %hwnd%,, x+xd, y+yd, s1, s2
	DllCall("BitBlt"
		, Ptr, hDCmem
		, "Int", 0
		, "Int", 0
		, "Int", t1
		, "Int", t2
		, Ptr, hDCtmp
		, "Int", x-cx
		, "Int", y-cy
		, "UInt", 0x00CC0020)
	if inv
		DllCall("BitBlt"
			, Ptr, hDCmem
			, "Int", 0
			, "Int", 0
			, "Int", t1
			, "Int", t2
			, Ptr, 0
			, "Int", 0
			, "Int", 0
			, "UInt", 0x00550009)
	DllCall("StretchBlt"
		, Ptr, hDCdest
		, "Int", 0
		, "Int", 0
		, "Int", zw
		, "Int", zh
		, Ptr, hDCmem
		, "Int", 0
		, "Int", 0
		, "Int", t1
		, "Int", t2
		, "UInt", 0x00CC0020)
	if hid
		{
		DllCall("ShowWindow", Ptr, hwnd, "UInt", 8)	; SW_SHOWNA
		hid=
		}
	}
DllCall("DeleteObject", Ptr, hRgn)
DllCall("DeleteObject", Ptr, DllCall("SelectObject", Ptr, hDCmem, Ptr, oBM))
DllCall("DeleteDC", Ptr, hDCmem)
DllCall("DeleteObject", Ptr, DllCall("SelectObject", Ptr, hDCtmp, Ptr, oBMtmp))
DllCall("DeleteDC", Ptr, hDCtmp)
DllCall("ReleaseDC", Ptr, hwnd, Ptr, hDCdest)
Tooltip,,,,20
;WinClose, ahk_id %hwnd%
}
;================================================================
ZoomUpdate(wP, lP)
;================================================================
{
Global
Static hwnd
if !lP
	{
	hwnd := ZoomStore(wP)
	return
	}
if (!zoom OR NumGet(lP+0, 0, Ptr) != hwnd)
	return
zoom=0
SetTimer, zoomon, -1
}
;================================================================
ZoomStore(idx, val="")
;================================================================
{
Static
ofi := A_FormatInteger
SetFormat, Integer, D
if val
	{
	h%idx% := val
	ZoomUpdate(idx, 0)
	r := TRUE
	}
else r:= h%idx%
SetFormat, Integer, %ofi%
return r
}
