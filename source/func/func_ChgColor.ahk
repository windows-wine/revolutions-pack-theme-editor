;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05,L1.1.13.0
;Depend:	updates.ahk
;Rqrmts:
;Date:	2014.04.28 03:51
;Version:	1.5
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

; proc: in/out R=ARGB, B=ABGR
; g=GUI number (1-99) or hWnd or window name
ChgColor(res="0x00FFFFFF", proc="RR", g="1")
{
Static
Global Ptr, AW
if !(hGUI := GetHwnd(g))
	{
	msgbox, Error in %A_ThisFunc%():`nInvalid GUI count/handle: %hGUI%.
	Return 0, ErrorLevel:="1" 
	}
StringSplit, p, proc
if !init
	{
	sz=36
	flags := 0x100 | 0x2 | 0x1	; CC_ANYCOLOR CC_FULLOPEN CC_RGBINIT
	VarSetCapacity(CHOOSECOLOR, sz, 0)
	f := "msvcrt\" (A_IsUnicode ? "swprintf" : "sprintf")
	VarSetCapacity(CustColors, 64)
	Loop, 16
		NumPut(0x00FFFFFF, CustColors, 4*(A_Index-1), "UInt")	; fix for NT-based systems
	init=1
	}
alpha := res & 0xFF000000
res &= 0x00FFFFFF
NumPut(sz, CHOOSECOLOR, 0, "UInt")
NumPut(hGUI, CHOOSECOLOR, 4, Ptr)
if p1=R
	res := SwColor(res)
NumPut(res, CHOOSECOLOR, 12, "UInt")
NumPut(&CustColors, CHOOSECOLOR, 16, Ptr)
NumPut(flags, CHOOSECOLOR, 20, "UInt")
r := DllCall("comdlg32\ChooseColor" AW, Ptr, &CHOOSECOLOR)
if (ErrorLevel || !r)
	return, ErrorLevel:="1"
;ofi := A_FormatInteger
;SetFormat, Integer, Hex
res := NumGet(CHOOSECOLOR, 12, "UInt")
if p2=R
	r := SwColor(res)
;else r:= res+0
;alpha+=0
r |= alpha
;SetFormat, Integer, %ofi%
DllCall(f, "Str", res, "Str", "0x%08X", "UInt", r)	; this is STUPIDLY FUCKED UP!!!
;msgbox, %res%
return res
}

SwColor(res)
{
ofi := A_FormatInteger 
SetFormat, Integer, Hex
r := ((res & 0xFF0000) >> 16) + (res & 0xFF00) + ((res & 0xFF) << 16)
SetFormat, Integer, %ofi%
return r
}

;#include func_GetHwnd.ahk
