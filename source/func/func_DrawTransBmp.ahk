; Drugwash, May 2014
; converts control coordinates from Window-relative to Client-relative
;================================================================
WP2CP(ctrl, ByRef x, ByRef y, ovr="")	; ovr: 0=use ctrl xy, 1=subtract from xy, 2=set xy
;================================================================
{
Global Ptr
ControlGetPos, cx, cy,,,, ahk_id %ctrl%
SetFormat, Integer, H
hwnd := DllCall("GetAncestor", Ptr, ctrl, "UInt", 2)	; GA_ROOT (PARENT=1, ROOTOWNER=3)
WinGet, ws, Style, ahk_id %hwnd%
WinGet, wsx, ExStyle, ahk_id %hwnd%
hMenu := DllCall("GetMenu", Ptr, hwnd)
SetFormat, Integer, D
SysGet, bx, 5	; SM_CYBORDER
SysGet, by, 6	; SM_CXBORDER
SysGet, ex, 5	; SM_CYEDGE
SysGet, ey, 6	; SM_CXEDGE
SysGet, fx, 7	; SM_CYFIXEDFRAME
SysGet, fy, 8	; SM_CXFIXEDFRAME
SysGet, sx, 32	; SM_CYFRAME
SysGet, sy, 33	; SM_CXFRAME
SysGet, c, 4	; SM_CYCAPTION
SysGet, m, 15	; SM_CYMENU
SysGet, sc, 51	; SM_CYSMCAPTION

tx := (ws & 0x40000 ? sx - ((ws & 0xC00000) ? 0 : bx) : 0) + (ws & 0x400000 ? (ws & 0x40000 ? 0 : fx) : 0)
	+ (ws & 0x800000 ? (ws & 0x440000 ? 0 : bx) : 0)
ty := (ws & 0x40000 ? sy - ((ws & 0xC00000) ? 0 : by) : 0) + (ws & 0x400000 ? (ws & 0x40000 ? 0 : fy) : 0)
	+ (ws & 0x800000 ? (ws & 0x440000 ? 0 : by) : 0) + (ws & 0xC00000=0xC00000 ? (wsx & 0x80 ? sc : c) : 0)
	+ (hMenu ? m : 0)
if !ovr
	x := cx-tx, y := cy-ty
else if ovr=1
	x -= tx, y -= ty
else if ovr=2
	x := tx, y := ty
return
msgbox,
(
bx=%bx% by=%by%
ex=%ex% ey=%ey%
fx=%fx% fy=%fy%
sx=%sx% sy=%sy%
c=%c% sc=%sc%
m=%m%
tx=%tx% ty=%ty%
ws=%ws%
wsx=%wsx%
hMenu=%hMenu%
)
}
/*
 +ToolWindow -Caption -Border +Resize
WS_POPUP		0x80000000
WS_VISIBLE		0x10000000
WS_CLIPSIBLINGS	0x04000000
WS_BORDER		0x00800000
WS_DLG_FRAME	0x00400000
WS_SYSMENU		0x00080000
WS_THICKFRAME	0x00040000 - size
WS_MINIMIZEBOX	0x00020000
WS_MAXIMIZEBOX	0x00010000 - size
WS_EX_WINDOWEDGE     0x100
WS_EX_TOOLWINDOW      0x80
*/
;================================================================
Sleep(t)
{
DllCall("winmm\timeBeginPeriod", "UInt", 3)
Loop, %t%
	DllCall("Sleep", "UInt", 1)
DllCall("winmm\timeEndPeriod", "UInt", 3)
}
;================================================================
DrawTransBmp(hPic, hSrc, p="", mask="A0", ppa="1")
;================================================================
{
Global Ptr, PtrP, AW, debug, hPbk
if hPic is not number
	{
	StringSplit, g, hPic, :
	if g2
		GuiControlGet, hPic, %g1%:Hwnd, %g2%
	else GuiControlGet, hPic, Hwnd, %g1%
	}
sx := sy := dx := dy := 0	; set origin point to top-left by default
ControlGetPos, cx, cy, dw, dh,, ahk_id %hPic%
WP2CP(hPic, cx, cy)
Loop, Parse, p, %A_Space%, %A_Space%%A_Tab%
	{
	StringLeft, i, A_loopField, 2
	StringTrimLeft, k, A_LoopField, 2
	if i in sx,sy,sw,sh,dx,dy,dw,dh,ix
		%i% := k
	}
if !(dw OR dh OR sw OR sh)
	return
StringLeft, t, mask, 1			; get transparency type (A=alpha, M=Magenta, C=color #)
StringTrimLeft, tv, mask, 1		; get transparency value (none for magenta)
SetFormat, Integer, H
hSrc+=0
if !(sw OR sh)
	{
	if !ix
		bpp := GetBmpSize(hSrc, sw, sh)
	else DllCall("ImageList_GetIconSize", Ptr, hSrc, PtrP, sw, PtrP, sh)
	}
; BLENDFUNCTION struct AC_SRC_OVER, PerPixelAlpha (ppa), global Alpha (tv)
BF := t="M" ? 0xFF00FF : t="C" ? tv : t="A" ? (~tv & 0xFF) << 16 | (ppa ? 0x1000000 : 0) : 0
L := "msimg32.dll", proc := (t="A" ? "AlphaBlend"  : "TransparentBlt")
if !API := GetProcAddress(proc, L)
	msgbox, % (lib ? "Procedure address " proc " cannot be found in " : "Missing library ") "msimg32.dll"
; set a null bitmap and delete previous hBmpTemp to avoid accumulation effect
DllCall("DeleteObject", Ptr, DllCall("SendMessage", Ptr, hPic, "UInt", 0x172, "UInt", 0, Ptr, 0))	; STM_SETIMAGE, IMAGE_BITMAP
DllCall("ShowWindow", Ptr, hPic, "UInt", 0)	; SW_HIDE
Sleep(10)		; Without this, Win98/7 gets a default background for the control DC sometimes
;VarSetCapacity(PS, 64, 0)	; PAINTSTRUCT struct
;hDC := DllCall("BeginPaint", Ptr, hPic, Ptr, &PS)
hDC := DllCall("GetDC", Ptr, hPic, Ptr)
hDC1 := DllCall("CreateCompatibleDC", Ptr, hDC, Ptr)	; memory DC
hBm1 := ix ? DllCall("CreateCompatibleBitmap" , Ptr, hDC, "Int", sw, "Int", sh, Ptr) : hSrc

hDC2 := DllCall("CreateCompatibleDC", Ptr, hDC, Ptr)	; temporary DC
hBm2 := DllCall("CreateCompatibleBitmap" , Ptr, hDC, "Int", dw, "Int", dh, Ptr)
hBo2 := DllCall("SelectObject", Ptr, hDC2, Ptr, hBm2)
hBo1 := DllCall("SelectObject", Ptr, hDC1, Ptr, hBm1)

hwnd := DllCall("GetAncestor", Ptr, hPic, "UInt", 2)	; GA_ROOT (PARENT=1, ROOTOWNER=3)
hBk := DllCall("GetDC", Ptr, hwnd, Ptr)
if ix
	{
; this isn't right as alpha doesn't work !!! Try ImageList_DrawEx()
	DllCall("BitBlt"
		, Ptr, hDC1
		, "Int", 0
		, "Int", 0
		, "Int", dw
		, "Int", dh
		, Ptr, hBk
		, "Int", cx
		, "Int", cy
		, "UInt", 0x00CC0020)	; SRCCOPY
	DllCall("ImageList_Draw", Ptr, hSrc, "Int", ix-1, Ptr, hDC1, "Int", 0, "Int", 0, "UInt", 1) ; ILD_TRANSPARENT
	}
else
	{
	DllCall("BitBlt"
		, Ptr, hDC2
		, "Int", 0
		, "Int", 0
		, "Int", dw
		, "Int", dh
		, Ptr, hBk
		, "Int", cx
		, "Int", cy
		, "UInt", 0x00CC0020)	; SRCCOPY
	}
DllCall("ReleaseDC", Ptr, hwnd, Ptr, hBk)
if !DllCall(API
	, Ptr, hDC2
	, "Int", 0
	, "Int", 0
	, "UInt", dw
	, "UInt", dh
	, Ptr, hDC1
	, "Int", sx
	, "Int", sy
	, "UInt", sw
	, "UInt", sh
	, "UInt", BF)
	msgbox, Error %ErrorLevel% in AlphaBlend()
DllCall("SelectObject", Ptr, hDC1, Ptr, hBo1)
if ix
	DllCall("DeleteObject", Ptr, hBm1)
DllCall("DeleteDC", Ptr, hDC1)

DllCall("BitBlt"
	, Ptr, hDC
	, "Int", dx
	, "Int", dy
	, "Int", dw
	, "Int", dh
	, Ptr, hDC2
	, "Int", 0
	, "Int", 0
	, "UInt", 0x00CC0020)	; SRCCOPY
if hP := DllCall("SendMessage" AW, Ptr, hPic, "UInt", 0x172, "UInt", 0, Ptr, hBm2)	; STM_SETIMAGE, IMAGE_BITMAP
	DllCall("DeleteObject", Ptr, hP)
DllCall("SelectObject", Ptr, hDC2, Ptr, hBo2)
DllCall("DeleteDC", Ptr, hDC2)
DllCall("ReleaseDC", Ptr, hPic, Ptr, hDC)
;DllCall("EndPaint", Ptr, hPic, Ptr, &PS)
WinSet, Redraw,, ahk_id %hPic%
DllCall("ShowWindow", Ptr, hPic, "UInt", 5)	; SW_SHOW
FreeLibrary(L)
SetFormat, Integer, D
if debug
GuiControl,, debug,
	(LTrim
	hPicture:`t%hPic% (%cx%x%cy%)
	hSource:`t%hSrc%
	Source size:`t%sw%x%sh%
	Dest. size:`t%dw%x%dh%
	hDC1:`t`t%hDC1%
	hDC2:`t`t%hDC2%
	hBm2:`t`t%hBm2%
	Lib. msimg32:`t%L%
	API handle:`t%API%
	procedure:`t%proc%
	BF:`t%BF% (ppa:%ppa% tv:%tv%)
	Image index:`t%ix%
	Parent: %hwnd%
	)
return hBm2
}
;================================================================
GetBmpSize(hObj, ByRef w, ByRef h)
{
Global Ptr, AW
sz=24
VarSetCapacity(buf, sz, 0)	; BITMAP struct
DllCall("GetObject" AW, Ptr, hObj, "UInt", sz, Ptr, &buf)
w := NumGet(buf, 4, "UInt")		; bmWidth
h := NumGet(buf, 8, "UInt")		; bmHeight
return NumGet(buf, 18, "UShort")	; bmBitsPixel
}
;================================================================
#include *i func_GetHwnd.ahk
#include *i func_ImageList.ahk
#include *i func_LibraryOp.ahk
