;==VERSIONING==>1.0
;About:	Helper functions intended to speed up repetitive library loading tasks
;Compat:	B1.0.48.05,L1.1.13.0
;Depend:	updates.ahk
;Rqrmts:	FreeLibrary() on exit for each loaded library
;Date:	2014.04.11 19:22
;Version:	1.0
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash, April 2014
;==VERSIONING==<
;================================================================
LoadLibrary(lib, t="")		; TO BE COMPLETED !!!
;================================================================
{
Global Ptr, AStr, AW
StringUpper, t, t
if !t, return DllCall("LoadLibrary" AW, "Str", lib, Ptr)			; free this handle on exit !!!
if t in A,W
	return DllCall("LoadLibrary" t, "Str", lib, Ptr)				; free this handle on exit !!!
if (InStr(t, "A")=1 OR InStr(t, "W")=1)
	StringTrimLeft, t1, t, 1
else t1 := AW
if !t, return DllCall("LoadLibrary" t1, "Str", lib, Ptr)			; free this handle on exit !!!
IfEqual,t,D, flags := 0x2			; LOAD_LIBRARY_AS_DATAFILE
return DllCall("LoadLibraryEx" t1, "Str", lib, Ptr, 0, "UInt", flags)
}
;================================================================
GetProcAddress(proc, ByRef lib, t="")
;================================================================
{
Global Ptr, AStr, AW
StringUpper, t, t
if t not in ,A,W
	return
if lib is integer
	mh := lib
else if !mh := DllCall("GetModuleHandle" AW, "Str", lib, Ptr)
	lib := mh := DllCall("LoadLibrary" AW, "Str", lib, Ptr)		; free this handle on exit !!!
if mh
	if !r := DllCall("GetProcAddress", Ptr, mh, AStr, proc t, Ptr)
		r := DllCall("GetProcAddress", Ptr, mh, AStr, proc AW, Ptr)
return r
}
;================================================================
FreeLibrary(lib)
;================================================================
{
Global Ptr
if lib is integer
	return !DllCall("FreeLibrary", Ptr, lib)
; we need to find library name and pass it back to 'lib' for future calls of GetProcAddress
}
