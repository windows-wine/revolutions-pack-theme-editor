;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05
;Depend:
;Rqrmts:
;Date:	2014.05.25 05:05
;Version:	1.1
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

; Simple array functions. Use var-compatible string for 'p' (4i2i6, 4_2_6)
; Name can be any name, it's only used for array identification
;================================================================
ArrayPut(name, p, val)
;================================================================
{
r := ArrayStore(name, p, TRUE, val)
return (r=val)
}
;================================================================
ArrayGet(name, p="")
;================================================================
{
return ArrayStore(name, p)
}
;================================================================
ArrayStore(n, p, a=0, v="")
;================================================================
{
Static
Static idx=0
i=
Loop, %idx%
	if (name%A_Index%=n)
		{
		i := A_Index
		break
		}
if !p
	return i
j := "path"
if InStr(p, j)=1
	{
	StringTrimLeft, tp, p, 4
	}
else j := p
if a
	{
	i := i ? i : ++idx
	name%i% := n
	if j=path
		p%i% := tp
	n%i%_%j% := v
	}
return n%i%_%j%
}
