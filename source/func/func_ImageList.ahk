;==VERSIONING==>
;About:	Collection of ImageList functions that allow custom icon sizes
;Compat:	B1.0.48.05,L1.1.13.0
;Date:	2014.05.10 11:50
;Version:	2.5
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash, February 2012-May 2014, Many thanks to SKAN for fixing bitmap stretch issue under XP
;==VERSIONING==<
;================================================================
; default: 16x16 ILC_COLOR24 ILC_MASK
; M=ILC_MASK P=ILC_PERITEMMIRROR I=ILC_MIRROR D=ILC_COLORDDB accepted colors: 4 8 16 24 32
ILC_Create(i, g="1", s="16x16", f="M24")
;================================================================
{
if i < 0
	return 0
StringSplit, s, s, x
s2 := s2 ? s2 : s1
c=
Loop, Parse, f
	if A_LoopField is digit
		c .= A_LoopField
StringReplace, f, f, %c%,,
m := c | (f="M" ? 0x1 : f="P" ? 0xA000 : f="I" ? 0x2000 : f="D" ? 0xFE : "0")
return DllCall("ImageList_Create", "Int", s1, "Int", s2, "UInt", m, "Int", i, "Int", g)
}
;================================================================
ILC_List(cx, file, idx="100", cd="D")	; cd=mask. D=Default (top-left px), N=none, 0xNNNNNN=color
;================================================================
{
Global Ptr, AW
mask := cd="D" ? 0xFF000000 : cd="N" ? 0xFFFFFFFF : cd
Loop, %file%
	if A_LoopFileExt in exe,dll
		{
		if !hInst := DllCall("GetModuleHandle" AW, "Str", file)
			hL := hInst := DllCall("LoadLibrary" AW, "Str", file)
		if idx is not integer
			i := &idx
		else i := idx
		hIL := DllCall("ImageList_LoadImage" AW, Ptr, hInst, "UInt", i, "Int", cx, "Int", 1, "UInt", mask, "UInt", 0, "UInt", 0x2000)
		}
	else if A_LoopFileExt in bmp
		hIL := DllCall("ImageList_LoadImage" AW, Ptr, 0, "Str", file, "Int", cx, "Int", 1, "UInt", mask, "UInt", 0, "UInt", 0x2010)
if (hInst && hL)
	DllCall("FreeLibrary", Ptr, hL)
return hIL
}
;================================================================
ILC_FitBmp(hPic, hIL, idx="1", tr="0")
;================================================================
{
Static
Global Ptr
PtrSize := A_PtrSize ? A_PtrSize : 4
WinGetPos,,, W1, H1, ahk_id %hPic%
if (W1 && H1)
	W := W1, H := H1
VarSetCapacity(IMAGEINFO, 24+2*PtrSize, 0)
DllCall("ImageList_GetImageInfo", Ptr, hIL, "Int", idx-1, Ptr, &IMAGEINFO)
bx := NumGet(IMAGEINFO, 8+2*PtrSize, "Int")
by := NumGet(IMAGEINFO, 12+2*PtrSize, "Int")
bw := NumGet(IMAGEINFO, 16+2*PtrSize, "Int")-bx
bh := NumGet(IMAGEINFO, 20+2*PtrSize, "Int")-by
;GuiControl,, Static1, Image %idx% @ %bx%.%by% size %bw%x%bh%
hDC := DllCall("CreateCompatibleDC", Ptr, 0)
hBm1 := DllCall("CreateBitmap" , "Int", bw, "Int", bh, "UInt", 1, "UInt", 0x18, "UInt", 0)
hBmp2 := DllCall("CopyImage", Ptr, hBm1, "UInt", 0, "Int", 0, "Int", 0, "UInt", 0x2008, Ptr)	; 0=IMAGE_BITMAP
DllCall("SelectObject", Ptr, hDC, Ptr, hBmp2)
DllCall("ImageList_Draw", Ptr, hIL, "Int", idx-1, Ptr, hDC, "Int", 0, "Int", 0, "UInt", tr) ; ILD_NORMAL/TRANSPARENT
DllCall("DeleteObject", Ptr, hBm1)
DllCall("DeleteDC", Ptr, hDC)
hBmp := DllCall("CopyImage", Ptr, hBmp2, "UInt", 0, "Int", W, "Int", H, "UInt", 0x2008, Ptr)	; 0=IMAGE_BITMAP
WinGetClass, cls, ahk_id %hPic%	; according to control class, use BM_SETIMAGE or STM_SETIMAGE, IMAGE_BITMAP
if hP := DllCall("SendMessage" AW, Ptr, hPic, "UInt", (cls="Button" ? 0xF7 : 0x172), "UInt", 0, Ptr, hBmp)
	DllCall("DeleteObject", Ptr, hP)
DllCall("DeleteObject", Ptr, hBmp2)
return hBmp
}
;================================================================
ILC_ImageResize(hIL, idx, sz="")
;================================================================
{
Global Ptr, PtrP
ofi := A_FormatInteger
SetFormat, Integer, H
DllCall("ImageList_GetIconSize", Ptr, hIL, PtrP, bw, PtrP, bh)
StringSplit, s, sz, x
s1 := s1 ? s1 : bw
s2 := s2 ? s2 : bh
hDC := DllCall("CreateCompatibleDC", Ptr, 0)
hBmp := DllCall("CreateBitmap" , "Int", bw, "Int", bh, "UInt", 1, "UInt", 0x18, "UInt", 0, Ptr)	; 1color plane, 24bit
hBmp2 := DllCall("CopyImage", Ptr, hBmp, "UInt", 0, "Int", 0, "Int", 0, "UInt", 0x2004, Ptr)	; 0=IMAGE_BITMAP
DllCall("DeleteObject", Ptr, hBmp)
hBo := DllCall("SelectObject", Ptr, hDC, Ptr, hBmp2)
DllCall("ImageList_Draw", Ptr, hIL, "Int", idx-1, Ptr, hDC, "Int", 0, "Int", 0, "UInt", 0x0) ; ILD_NORMAL
DllCall("SelectObject", Ptr, hDC, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDC)
hBmp := ResizeBmp(hBmp2, s1, s2)
DllCall("DeleteObject", Ptr, hBmp2)
SetFormat, Integer, %ofi%
return hBmp
}
;================================================================
ResizeBmp(hBmp, w, h="")
;================================================================
{
Global Ptr
h := h ? h : w
if hBmp	; LR_CREATEDIBSECTION LR_COPYRETURNORG
	return DllCall("CopyImage", Ptr, hBmp, "UInt", 0, "Int", w, "Int", h, "UInt", 0x2004, Ptr)	; 0=IMAGE_BITMAP
; requires cleanup on exit!
}
;================================================================
; sz=individual image width, file=file path or ImageList handle
; res=resource index in file, cd=color depth (1-32bit, 0-lower)
GetBmp(idx, sz, ByRef file, res="", cd="D")
;================================================================
{
if file is integer
	hIL := file
else IfExist, %file%
	{
	if !hIL := ILC_List(sz, file, res, cd)
		return 0
	file := hIL
	}
else return 0
ofi := A_FormatInteger
SetFormat, Integer, H
file+=0
hIL+=0
SetFormat, Integer, %ofi%
return ILC_ImageResize(hIL, idx, sz)
}
;================================================================
GetPixelColor(hBmp, px, py)
;================================================================
{
Global Ptr
ofi := A_FormatInteger
SetFormat, Integer, H
hDC := DllCall("CreateCompatibleDC", Ptr, 0)
hBo := DllCall("SelectObject", Ptr, hDC, Ptr, hBmp)
sz := DllCall("GetPixel", Ptr, hDC, "Int", px, "Int", py, Ptr)
DllCall("SelectObject", Ptr, hDC, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDC)
SetFormat, Integer, %ofi%
return sz
}
;================================================================
GetBmpInfo(file, ByRef bw, ByRef bh, ByRef bpp)
;================================================================
{
Global Ptr, AW
if InStr(file, "|")
	{
	StringSplit, f, file, |
	file := f1, idx := f2
	}
Loop, %file%
	if A_LoopFileExt in exe,dll
		{
		if (idx="")
			return 0
		if !hInst := DllCall("GetModuleHandle" AW, "Str", file)
			hL := hInst := DllCall("LoadLibraryEx" AW, "Str", file, "UInt", 0, "UInt", 0x2)
			; LOAD_LIBRARY_AS_DATAFILE. Don't use DONT_RESOLVE_DLL_REFERENCES !!!
		; need to use MAKEINTRESOURCE here
		if idx is not integer
			i := &idx
		else i := idx
		hBmp := DllCall("LoadImage" AW, Ptr, hInst, "UInt", i, "UInt", 0, "Int", 0, "Int", 0, "UInt", 0x2000)
		}
	else if A_LoopFileExt in bmp
		hBmp := DllCall("LoadImage" AW, Ptr, 0, "Str", file, "UInt", 0, "Int", 0, "Int", 0, "UInt", 0x2010)
if hL
	DllCall("FreeLibrary", Ptr, hL)
if !hBmp
	return 0
VarSetCapacity(buf, sz := 24, 0)	; BITMAP struct (DIBSECTION is 84, not needed)
DllCall("GetObject", Ptr, hBmp, "UInt", sz, Ptr, &buf)
bw := NumGet(buf, 4, "Int")
bh := NumGet(buf, 8, "Int")
bpp := NumGet(buf, 18, "UShort")
DllCall("DeleteObject", Ptr, hBmp)
VarSetCapacity(buf, 0)			; Free buffer memory
}
;================================================================
SetBmp(hDest, hBmp)
;================================================================
{
Global Ptr
return DllCall("SendMessage" AW, Ptr, hDest, "UInt", 0x172, "UInt", 0, Ptr, hBmp)	; STM_SETIMAGE, IMAGE_BITMAP
}
;================================================================
ILC_Count(hwnd)
;================================================================
{
Global Ptr
if hwnd
	return DllCall("ImageList_GetImageCount", Ptr, hwnd)
}
;================================================================
ILC_Destroy(hwnd)
;================================================================
{
Global Ptr
return DllCall("ImageList_Destroy", Ptr, hwnd)
}
;================================================================
; LR_CREATEDIBSECTION=0x2000 LR_LOADFROMFILE=0x10 LR_LOADTRANSPARENT=0x20 LR_SHARED=0x8000
; IMAGE_BITMAP=0x0 IMAGE_ICON=0x1 IMAGE_CURSOR=0x2
ILC_Add(hIL, icon, idx="1", mask="")
;================================================================
{
Global Ptr, AW
Static it="BIC"
StringLeft, t, icon, 1
StringTrimLeft, icon, icon, 1
t := InStr(it, t)-1
if (t<0 OR hIL=0)	; this one had it broken forever !!!
	return 0
hInst=0
mask := (mask & 0xFFFFFFFF)!="" ? mask : ""
if icon is integer
	hIcon := icon
else
	{
	Loop, %icon%
		if A_LoopFileExt in exe,dll
			{
			if !hInst := DllCall("GetModuleHandle" AW, "Str", icon)
;				hL := hInst := DllCall("LoadLibrary" AW, "Str", icon)
				hL := hInst := DllCall("LoadLibraryEx" AW, "Str", icon, "UInt", 0, "UInt", 0x2)
				; LOAD_LIBRARY_AS_DATAFILE. Don't use DONT_RESOLVE_DLL_REFERENCES !!!
			flags=0x2000
		; need to use MAKEINTRESOURCE here
			if idx is not integer
				i := &idx
			else i := idx
			hIcon := DllCall("LoadImage" AW, Ptr, hInst, "UInt", i, "UInt", t, "Int", 0, "Int", 0, "UInt", flags)
			}
		else if A_LoopFileExt in bmp,ico,cur,ani
			{
			flags=0x2010
			hIcon := DllCall("LoadImage" AW, Ptr, hInst, "Str", icon, "UInt", t, "Int", 0, "Int", 0, "UInt", flags)
			}
		else
			{
			i := idx
			flags=0x8000
			hIcon := DllCall("LoadImage" AW, Ptr, -1, "UInt", i, "UInt", t, "Int", 0, "Int", 0, "UInt", flags)
			}
	}
if (hInst && hL)
	DllCall("FreeLibrary", Ptr, hInst)
if t=0
	{
	if (mask != "")
		r := DllCall("ImageList_AddMasked", Ptr, hIL, Ptr, hIcon, "UInt", mask)
	else r := DllCall("ImageList_Add", Ptr, hIL, Ptr, hIcon, "UInt", 0)
	DllCall("DeleteObject", Ptr, hIcon)
	}
if t=1
	{
	r := DllCall("ImageList_ReplaceIcon", Ptr, hIL, "UInt", -1, Ptr, hIcon)
	DllCall("DestroyIcon", Ptr, hIcon)
	}
if t=2
	{
	r := DllCall("ImageList_ReplaceIcon", Ptr, hIL, "UInt", -1, Ptr, hIcon)
	DllCall("DestroyCursor", Ptr, hIcon)
	}
return r
}
;================================================================
ILC_GetIcon(hIL, idx, f=0x1)
;================================================================
{
Global Ptr
if f < 0x10		; ILD_NORMAL/TRANSPARENT/FOCUS/BLEND/MASK (default: transparent)
	return DllCall("ImageList_GetIcon", Ptr, hIL, "Int", idx, "UInt", f)
return 0
}
;================================================================
; we need to add ImageList_AddMasked() to work with Magenta transparency (for RP9 and others)
